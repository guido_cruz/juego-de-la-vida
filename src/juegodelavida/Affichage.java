package juegodelavida;

import java.awt.Dimension;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Component;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import java.awt.Panel;

// 
// Decompiled by Procyon v0.5.36
// 

public class Affichage extends Panel
{
    private static final int MAX_RESOLUTION = 10;
    private static final int MAX_SIZE = 700;
    private static JFrame frame;
    private static Affichage world;
    private static BufferedImage image;
    private static boolean init;
    private static int resolution;
    private static int sleepTime;
    private BufferedImage worldImage;
    private boolean[][] grille;
    private int xSize;
    private int ySize;
    
    public Affichage(final boolean[][] grille) {
        this.grille = grille;
        this.xSize = grille.length;
        this.ySize = grille[0].length;
        if (Affichage.resolution <= 0) {
            Affichage.resolution = 700 / grille.length;
            if (Affichage.resolution <= 0) {
                Affichage.resolution = 1;
            }
            else if (Affichage.resolution > 10) {
                Affichage.resolution = 10;
            }
        }
        (Affichage.frame = new JFrame("Le jeu de la vie de Conway")).setSize(Affichage.resolution * this.xSize + 2, Affichage.resolution * this.ySize + 2);
        Affichage.frame.setDefaultCloseOperation(3);
        Affichage.image = new BufferedImage(Affichage.resolution * this.xSize + 2, Affichage.resolution * this.ySize + 2, 1);
        Affichage.world = new Affichage(Affichage.image);
        Affichage.frame.add(Affichage.world);
        Affichage.frame.pack();
        this.afficherMonde();
        Affichage.frame.setVisible(true);
    }
    
    private void drawCell(final Graphics graphics, final int n, final int n2) {
        if (this.grille[n][n2]) {
            graphics.fillRect(Affichage.resolution * n2 + 1, Affichage.resolution * n + 1, Affichage.resolution, Affichage.resolution);
        }
    }
    
    public void setGrille(final boolean[][] grille) {
        this.grille = grille;
        this.afficherMonde();
    }
    
    private void afficherMonde() {
        final Graphics graphics = Affichage.image.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, Affichage.resolution * this.xSize + 2, Affichage.resolution * this.ySize + 2);
        graphics.setColor(Color.GRAY);
        graphics.drawRect(0, 0, Affichage.resolution * this.xSize + 1, Affichage.resolution * this.ySize + 1);
        graphics.setColor(Color.BLACK);
        for (int i = 0; i < this.xSize; ++i) {
            for (int j = 0; j < this.ySize; ++j) {
                this.drawCell(graphics, i, j);
            }
        }
        Affichage.world.paint(Affichage.world.getGraphics());
        try {
            Thread.sleep(Affichage.sleepTime);
        }
        catch (InterruptedException ex) {}
    }
    
    public static void setDelay(final int sleepTime) {
        Affichage.sleepTime = sleepTime;
    }
    
    private static void setResolution(int n) {
        n = n;
    }
    
    private Affichage(final BufferedImage worldImage) {
        this.worldImage = worldImage;
    }
    
    @Override
    public void paint(final Graphics graphics) {
        graphics.drawImage(this.worldImage, 0, 0, this);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(this.worldImage.getWidth(), this.worldImage.getHeight());
    }
    
    static {
        Affichage.frame = null;
        Affichage.world = null;
        Affichage.image = null;
        Affichage.init = false;
        Affichage.resolution = 0;
        Affichage.sleepTime = 500;
    }
}