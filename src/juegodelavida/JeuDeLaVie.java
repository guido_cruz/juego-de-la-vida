/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegodelavida;

/**
 *
 * @author Guido Cruz
 */
public class JeuDeLaVie {
        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean[][] tab = { {false, false, false, false, false, false, false, false, false},
                            {false, false, false, true, true, true, false, false, false},
                            {false, false, false, false, false, false, false, false, false},
                            {false, true, false, false, false, false, false, true, false},
                            {false, true, false, false, false, false, false, true, false},
                            {false, true, false, false, false, false, false, true, false},
                            {false, false, false, false, false, false, false, false, false},
                            {false, false, false, true, true, true, false, false, false},
                            {false, false, false, false, false, false, false, false, false}};

        
        // el algoritmo se movio a la clase Mundo
        //pero esta vez no hace los print, por que tiene que retornarnos 
        //en una variable String.
        Monde tablero_prueba = new Monde(tab, 10);
        String paraMostrar = tablero_prueba.toString();
        System.out.println(paraMostrar);
         
        //la cantidad de vecinos vivos para la posicion 1, 1 es 0
        int cantidadVecinosVivos = tablero_prueba.nombreDeVoisins(1, 1);
        System.out.println(String.format("cantidad vecinos VIVOS para [1, 1] %d", cantidadVecinosVivos));
    
        //la cantidad de vecinos vivos para la posicion 2, 1 es 1
        cantidadVecinosVivos = tablero_prueba.nombreDeVoisins(2, 1);
        System.out.println(String.format("cantidad vecinos VIVOS para [2, 1] %d", cantidadVecinosVivos));
        
        //la cantidad de vecinos vivos para la posicion 3, 1 es 1
        cantidadVecinosVivos = tablero_prueba.nombreDeVoisins(3, 1);
        System.out.println(String.format("cantidad vecinos VIVOS para [3, 1] %d", cantidadVecinosVivos));
        
        //la cantidad de vecinos vivos para la posicion 4, 3 es 0
        cantidadVecinosVivos = tablero_prueba.nombreDeVoisins(4, 3);
        System.out.println(String.format("cantidad vecinos VIVOS para [4, 3] %d", cantidadVecinosVivos));

        tablero_prueba.faireEvoluer();
        System.out.println(tablero_prueba);
        tablero_prueba.faireEvoluer();
        System.out.println(tablero_prueba);
        
        for (int i = 0; i < 20; i++) {
            tablero_prueba.faireEvoluer();
            System.out.println(tablero_prueba);
        }
        
        //Affichage afichage = new Affichage(tab);
    }
}
