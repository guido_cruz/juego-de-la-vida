package juegodelavida;

public class Monde {

    private int numOrdre;
    private int nbVies;
    private boolean[][] tab;

    private int edad;

    //constructeurs
    public Monde(int dim1, int dim2, int num) {
        //A completer
    }

    public Monde(boolean[][] tab, int num) {
        this.tab = tab;
        numOrdre = num;
        this.edad = 0;
        int celulasVivas = this.getCantidadCelulasVicas();
        System.out.println("celulas vivas: " + celulasVivas);
    }

    //getters
    public int getNbvies() {
        return nbVies;
    }

    public boolean[][] getGrille() {
        return tab;
    }

    /**
     * Affiche la grille de cellules
     */
    public String toString() {
        String resultado = "";
        //algoritmo para mostrar la figura 2
        for (int i = 0; i < this.tab.length; i++) {
            for (int j = 0; j < this.tab[0].length; j++) {
                if (this.tab[i][j]) {
                    resultado = resultado + "|O";
                } else {
                    resultado = resultado + "|X";
                }
            }
            resultado = resultado + "|\n";
        }
        return resultado;
        //fin de algoritmo
    }

    /**
     * Réactualise le nombre de vies d'un monde
     */
    public void updateNbVies() {

        int cptnbVies = this.getCantidadCelulasVicas();
        // A compléter
        nbVies = cptnbVies;

    }

    /**
     * Teste si toutes les cellules sont mortes
     */
    public boolean estVide() { //esta vacio
        return (nbVies == 0);
    }

    /**
     * Teste si une cellule située aux coordonnées i et j sera vivante à
     * l'évolution
     *
     * @param ligne numéro de ligne
     * @param colonne numéro de colonne
     */
    private boolean celluleDevientVivante(int ligne, int colonne) { //celular se vuelve vivo

        boolean vivant = false;
        // A compléter
        if (this.nombreDeVoisins(ligne, colonne) == 2
                || this.nombreDeVoisins(ligne, colonne) == 3) {
            vivant = true;
        }
        return vivant;
    }

    public boolean[][] faireEvoluer() {
        /*
        • muere una célula viva con menos de dos vecinos (subpoblación),
        • sobrevive una célula viva con dos o tres vecinos,
        • muere una célula viva con más de tres vecinos (sobrepoblación),
        • una célula muerta con exactamente tres vecinos cobra vida (reproducción).
         */
        boolean[][] mundoEvolucionado = new boolean[tab.length][tab[0].length];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                if (tab[i][j]) {
                    if (celluleDevientVivante(i, j)) {
                        mundoEvolucionado[i][j] = true;
                    } else {
                        mundoEvolucionado[i][j] = false;
                    }
                } else {
                    if (nombreDeVoisins(i, j) == 3) {
                        mundoEvolucionado[i][j] = true;
                    } else {
                        mundoEvolucionado[i][j] = false;
                    }
                }

            }
        }
        this.tab = mundoEvolucionado;
        this.edad = this.edad + 1;
        System.out.println("cantidad de celulas despues de la evolucion:" + this.getCantidadCelulasVicas());
        return tab;
    }

    /**
     * Calcul du nombre de voisins d'une cellule
     *
     * @param ligne numéro de ligne
     * @param colonne numéro de colonne
     */
    public int nombreDeVoisins(int ligne, int colonne) { //nro. de vecinos
        //vecinos de la posicion x,y...
        /*  
            x-1, y-1 | x-1, y | x-1, y+1
            ------------------------------
            x  , y-1 | x  , y | x  , y+1
            ------------------------------
            x+1, y-1 | x+1, y | x+1, y+1
        
        en este tablero pequenhio , la posicion x,y tiene 8 vecinos.
         */
        int cantidadVecinosVivos = 0;
        if (esPosicionValida(ligne, colonne, this.tab)) {
            //recorremos toda la tabla, miramos a i representando a x
            //y a la variable j representando a y.
            for (int i = 0; i < this.tab.length; i++) {
                for (int j = 0; j < this.tab[0].length; j++) {
                    //con este 'if' nos encargamos de tomar en cuenta solo a los vecinos,
                    //solo los que tienen las posiciones basadas en los valores de x,y que es nuestro punto de enfoque.
                    if (esVecino(ligne, colonne, i, j)) {
                        //incrementamos cantidad vecinos
                        //solo si este esta vivo, osea el valor de la celda es 'true'
                        if (this.tab[i][j]) {
                            cantidadVecinosVivos++;
                        }
                    }
                }
            }
        }
        return cantidadVecinosVivos;
    }

    public int getCantidadCelulasVicas() {
        int resultado = 0;
        for (int i = 0; i < this.tab.length; i++) {
            for (int j = 0; j < this.tab[0].length; j++) {
                //incrementamos si el valor de esta celda es true, que singnifica que 
                //tiene una celula viva
                if (this.tab[i][j]) {
                    resultado++;
                }
            }
        }
        return resultado;
    }

    //Metodo que nos dice la cantidad de vecinso vivos dado una tabla, 
    //y la posicion de la que vamos a revisar sus vecinos

    //metodo que nos avisa si es un vecino o no lo es, este metodo retorna un valor boolean
    //true si es y false si no lo es.
    //es un vecino solo si la posicion esta a lado de la posicion original que le damos.
    //este metodo recibe 4 parametros yOrig, yOrig que es nuestra posicion de referencia
    //y xPosible, yPosible es la posicion a verificar si es o no es vecino de los yOrig, yOrig
    public boolean esVecino(int xOrig, int yOrig, int xPosible, int yPosible) {
        /*  
            x-1, y-1 | x-1, y | x-1, y+1
            ------------------------------
            x  , y-1 | x  , y | x  , y+1
            ------------------------------
            x+1, y-1 | x+1, y | x+1, y+1
        
        este es una tabla de los que SI son posiciones de los vecinos para x,y.
         */
        if (esPosicionValida(xPosible, yPosible, this.tab)) {
            if ((xPosible == xOrig - 1 || xPosible == xOrig || xPosible == xOrig + 1)
                    && (yPosible == yOrig - 1 || yPosible == yOrig || yPosible == yOrig + 1)
                    && !(xPosible == xOrig && yPosible == yOrig)) {
                //mostramos en la consola si es vecino, se puede eliminar esta linea si no nos sirve.
                //System.out.println(String.format("vecino: %s, %s", xPosible, yPosible));
                return true;
            }
        }
        return false;
    }

    //metodo que nos avisa con un true o false, verificando si el valor de x, y que le pasamos 
    //por los parametros de entrada son validos en el tablero que tambien le mandamos.
    //en caso de enviarle -1, 0, automaticamente no es una posicion invalida por que no existe la
    //posicion -1, esto puede llegar a pasar cuando se esta verificando si es vecino o no, en ese metodo 
    //hace sumas y restas con 1. y si nuestra posicion es 0,0 al restar tendremos -1, y el otro extremo cuando
    //tengamos como posicion el ultimo elemento en el tablero, al sumar +1, tendremos una posicion que no existe en la tabla.
    public boolean esPosicionValida(int x, int y, boolean[][] tablero) {
        if (x >= 0 && x < tablero.length && y >= 0 && y < tablero[0].length) {
            return true;
        } else {
            //System.out.println(String.format("posicion invalida: %s %s", x, y));
            return false;
        }
    }

    public boolean equals(boolean[][] mundoA, boolean[][] mundoB) {
        boolean equals = true;
        //verificamos de que los tamanios sean iguales, en caso de que falle, retorna false.
        if ((mundoA.length != mundoB.length)
                || (mundoA[0].length != mundoB[0].length)) {
            return false;
        }
        //comparamos el valor de cada elemento, basta que uno no sea igual, retornamos false.
        for (int i = 0; i < mundoA.length; i++) {
            for (int j = 0; j < mundoA[0].length; j++) {
                if (mundoA[i][j] != mundoB[i][j]) {
                    return false;
                }
            }
        }
        return equals;
    }
}
